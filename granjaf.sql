-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema granja
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema granja
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `granja` DEFAULT CHARACTER SET utf8 ;
USE `granja` ;

-- -----------------------------------------------------
-- Table `granja`.`sexo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `granja`.`sexo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sexo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `granja`.`salud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `granja`.`salud` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `salud` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `granja`.`animales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `granja`.`animales` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `animales` VARCHAR(45) NOT NULL,
  `edad` INT NOT NULL,
  `visibilidad` TINYINT NOT NULL DEFAULT 1,
  `fecha` DATETIME NOT NULL,
  `sexo_id` INT NOT NULL,
  `salud_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_animales_sexo_idx` (`sexo_id` ASC) VISIBLE,
  INDEX `fk_animales_salud1_idx` (`salud_id` ASC) VISIBLE,
  CONSTRAINT `fk_animales_sexo`
    FOREIGN KEY (`sexo_id`)
    REFERENCES `granja`.`sexo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animales_salud1`
    FOREIGN KEY (`salud_id`)
    REFERENCES `granja`.`salud` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
